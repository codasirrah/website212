const btn = document.querySelector('.btn-animated')

const bool = true;
btn.addEventListener('click', () => {
    if (bool) {
        btn.classList.add('btn-animation');
        btn.addEventListener('animationend', () => {
            btn.classList.remove('btn-animation');
        })
        return bool = false;
    }
})